class Ranges {
    constructor() {
        this.ranges = [];
    }

    add(range) {
        if (!Array.isArray(range)) return;
        range.length = 2;

        this.ranges.push(range);
        this.ranges.sort((a, b) => a[0] - b[0]);

        for (let i = 0; i < this.ranges.length - 1; i++) {
            const currentRange = this.ranges[i];
            const nextRange = this.ranges[i + 1];

            if (!isNumberInRange(currentRange, nextRange[0])) continue;

            this.ranges[i][1] = currentRange[1] >= nextRange[1] ? currentRange[1] : nextRange[1];
            this.ranges.splice(i + 1, 1);
            --i;
        }
    }

    remove(range) {
        if (!Array.isArray(range)) return;
        range.length = 2;

        for (let i = 0; i < this.ranges.length; i++) {
            if (this.ranges[i][0] > range[1]) return;

            const newRanges = removeFromRange(this.ranges[i], range);
            if (newRanges && newRanges.length) {
                this.ranges.splice(i, 1, ...newRanges);
            } else if (newRanges) {
                this.ranges.splice(i, 1);
                --i;
            }
        }
    }

    print() {
        console.log(JSON.stringify(this.ranges));
        return this.ranges;
    }
}

function removeFromRange(range, remove) {
    const newRanges = [];
    let removeRange;
    if (range[0] < remove[0] && range[1] >= remove[0]) {
        newRanges.push([range[0], remove[0] - 1]);
        range[0] = remove[0];
    }

    if (range[0] > remove[0]) remove[0] = range[0];

    if (range[0] === remove[0]) {
        while (range[0] <= range[1] && range[0] <= remove[1]) {
            removeRange = true;
            ++range[0];
            ++remove[0];
        }
    }

    if (range[0] <= range[1]) newRanges.push(range);

    return newRanges.length ? newRanges : removeRange;
}

function isNumberInRange(range, number) {
    return range[0] - 1 <= number && range[1] + 1 >= number;
}

const r = new Ranges();
r.add([1, 4]);
r.print();
// Should display: [1, 4]
r.add([10, 20]);
r.print();
// Should display: [1, 4] [10, 20]
r.add([10, 10]);
r.print();
// Should display: [1, 4] [10, 20]
r.add([21, 21]);

r.print();
// Should display: [1, 4] [10, 21]
r.add([2, 4]);
r.print();
// Should display: [1, 4] [10, 21]
r.add([3, 8]);
r.print();
// Should display: [1, 8] [10, 21]
r.remove([10, 10]);
r.print();
// Should display: [1, 8] [11, 21]
r.remove([10, 11]);
r.print();
// Should display: [1, 8] [12, 21]

r.remove([15, 17]);
r.print();
// Should display: [1, 8] [12, 14] [18, 21]

r.remove([3, 19]);
r.print();
// Should display: [1, 2] [20, 21]